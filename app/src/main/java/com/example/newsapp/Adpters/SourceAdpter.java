package com.example.newsapp.Adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsapp.Model.Sources;
import com.example.newsapp.R;
import com.example.newsapp.Refrofit.Helper;
import com.example.newsapp.interfaces.SourceeSelectedListner;

import java.util.ArrayList;
import java.util.List;

public class SourceAdpter extends RecyclerView.Adapter<SourceAdpter.SourceHolder> implements Filterable {

    private List<Sources> contactListFiltered;
    private List<Sources> list;
    private final Context context;
    private final SourceeSelectedListner listner;

    public SourceAdpter(Context context, List<Sources> list, SourceeSelectedListner listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }

    @NonNull
    @Override
    public SourceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.source_temp, viewGroup, false);
        return new SourceHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SourceHolder sourceHolder, int i) {
        final Sources sources = list.get(i);
        sourceHolder.textdes.setText(sources.description);
        sourceHolder.textname.setText(sources.name);
        sourceHolder.itemView.setOnClickListener(v -> {
            if (Helper.isNetworkConnected(context))
                  listner.onSourceSelected(sources);
            else {
                Toast.makeText(context, "no internet", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return getFilter;
    }

    Filter getFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            String filterpattern = charSequence.toString().toLowerCase();

            List<Sources> filterdlist = new ArrayList<>();

            if (filterpattern.length() == 0 && filterpattern.isEmpty())
                filterdlist.addAll(contactListFiltered);

            for (Sources sources : contactListFiltered) {
                if (sources.name.toLowerCase().contains(filterpattern))
                    filterdlist.add(sources);
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filterdlist;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            list.clear();
            list.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public static class SourceHolder extends RecyclerView.ViewHolder {
        TextView textname;
        TextView textdes;

        public SourceHolder(@NonNull View itemView) {
            super(itemView);
            textname = itemView.findViewById(R.id.sourceName);
            textdes = itemView.findViewById(R.id.Source_discription);

        }
    }

    public void updateData(List<Sources> list) {
        this.list = list;
        this.contactListFiltered = new ArrayList<>(list);
        notifyDataSetChanged();
    }

}
