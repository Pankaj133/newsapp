package com.example.newsapp.Adpters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.newsapp.Model.Category;
import com.example.newsapp.R;
import com.example.newsapp.interfaces.CategorySelectedListner;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {


    private final Context context;
    private final List<Category> list =  new ArrayList<>();
    CategorySelectedListner listner ;
    public CategoryAdapter(Context context,CategorySelectedListner listner) {
        this.context = context;
        this.listner = listner;
        createList();
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.category_temp,viewGroup,false);
        return new CategoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder categoryHolder, int i) {
        final Category  category = list.get(i);
        categoryHolder.catimage.setImageResource(category.img);
        categoryHolder.cattext.setText(category.name);
        categoryHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.onCategorySelected(category.name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  static  class CategoryHolder extends RecyclerView.ViewHolder {
        ImageView catimage;
        TextView cattext;
        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            catimage = itemView.findViewById(R.id.category_img);
            cattext = itemView.findViewById(R.id.categoryName);

        }
    }

    private void createList() {
        /* ADDING CATEGORIES */
        list.add(new Category("Technology",R.drawable.tech));
        list.add(new Category("Sports",R.drawable.sports));
        list.add(new Category("Business",R.drawable.bussiness));
        list.add(new Category("Entertainment",R.drawable.enter));
        list.add(new Category("General",R.drawable.general));
        list.add(new Category("Health",R.drawable.health));
        list.add(new Category("Science",R.drawable.science));
    }
}
