package com.example.newsapp.Adpters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.newsapp.Detail;
import com.example.newsapp.Model.Article;
import com.example.newsapp.R;
import com.example.newsapp.Refrofit.Helper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class NewsAdpter extends RecyclerView.Adapter<NewsAdpter.NewsViewHolder> implements Filterable {

    private final List<Article> art_list;
    private final Context context;
    private final List<Article> filternewslist;

    public NewsAdpter(Context context, List<Article> art_list) {
        this.context = context;
        this.art_list = art_list;
        filternewslist = new ArrayList<>(art_list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.news_temp, viewGroup, false);

        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int i) {

        final Article article = art_list.get(i);
        newsViewHolder.texttitle.setText(article.title);
        newsViewHolder.textdescription.setText(article.description);
        try {
            newsViewHolder.texthour.setText(Helper.convertTimeStamp(article.publishedAt));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Glide.with(context)
                .load(article.urlToImage)
                .into(newsViewHolder.image);

        newsViewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, Detail.class);
            intent.putExtra("Url", article.url);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return art_list.size();
    }

    @Override
    public Filter getFilter() {
        return newsfilter;
    }

    Filter newsfilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Article> filterdlist = new ArrayList<>();

            String filterpatter = constraint.toString().toLowerCase();

            if (filterpatter.length() == 0 && filterpatter.isEmpty()) {
                filterdlist.addAll(filternewslist);
            }

            for (Article article : filternewslist) {
                if (article.title.toLowerCase().contains(filterpatter))
                    filterdlist.add(article);
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filterdlist;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            art_list.clear();
            art_list.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView texttitle, textdescription, texthour;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.news_image);
            texttitle = itemView.findViewById(R.id.news_title);
            textdescription = itemView.findViewById(R.id.news_description);
            texthour = itemView.findViewById(R.id.hour);
        }
    }
}
