package com.example.newsapp.Model;

import com.google.gson.annotations.SerializedName;

public class Article {

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("url")
    public String url;

    @SerializedName("urlToImage")
    public String urlToImage;

    @SerializedName("publishedAt")
    public String publishedAt;

    @SerializedName("content")
    public String content;
}
