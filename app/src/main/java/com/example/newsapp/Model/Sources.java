package com.example.newsapp.Model;

import com.google.gson.annotations.SerializedName;

public class Sources {


    @SerializedName("id")
    public  String id;

    @SerializedName("name")
    public  String name;

    @SerializedName("description")
    public String description;

}
