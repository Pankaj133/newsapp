package com.example.newsapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsList {

    @SerializedName("articles")
    public List<Article> articles;
}
