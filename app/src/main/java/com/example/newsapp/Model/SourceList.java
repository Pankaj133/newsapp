package com.example.newsapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SourceList {

    @SerializedName("sources")
    public List<Sources> sources;
}
