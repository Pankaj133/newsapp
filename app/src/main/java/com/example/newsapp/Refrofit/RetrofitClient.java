package com.example.newsapp.Refrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {


    public static String apikey = "d9d235ae5067407e976b84a5b1195cd0";
    //public static String apikey = "01a17df23ab440b49e13cbad4a53cd7a";

    public static Retrofit retrofit;

    public static Retrofit getRetrofit(String base_url) {

        retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
