package com.example.newsapp;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsapp.Adpters.NewsAdpter;
import com.example.newsapp.Model.Article;
import com.example.newsapp.Model.NewsList;
import com.example.newsapp.Refrofit.Comman;
import com.example.newsapp.Refrofit.Helper;
import com.example.newsapp.Refrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends AppCompatActivity {

    private RecyclerView news_recycler;
    private String id, sourceName;
    private NewsAdpter adpter;
    private List<Article> list = new ArrayList<>();
    private Toolbar toolbar;
    private TextView toolbar_title;
    private SearchView newsearchview;
    private RelativeLayout content;
    private LinearLayout newointernetlayout, newloading_layout;
    private TextView realoadbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        news_recycler = findViewById(R.id.News_recycler);
        news_recycler.setLayoutManager(new LinearLayoutManager(this));
        content = findViewById(R.id.newscontent);
        newointernetlayout = findViewById(R.id.no_net);
        newloading_layout = findViewById(R.id.loadinglayout);
        realoadbutton = findViewById(R.id.realloadbutton);
        newsearchview = findViewById(R.id.newssearchview);

        newsearchview.setMaxWidth(Integer.MAX_VALUE);

        newsearchview.setOnQueryTextFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                toolbar_title.setVisibility(View.GONE);
            else
                toolbar_title.setVisibility(View.VISIBLE);
        });

        id = getIntent().getStringExtra("sourceId");
        newsData();
        setuptoolbar();

        newsearchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(Helper.isNetworkConnected(NewsActivity.this))
                    adpter.getFilter().filter(s);
                else
                    Toast.makeText(NewsActivity.this, "no internet", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(Helper.isNetworkConnected(NewsActivity.this))
                    adpter.getFilter().filter(s);

                return false;
            }
        });
        realoadbutton.setOnClickListener(v -> newsData());

    }


    private void setuptoolbar() {
        toolbar = findViewById(R.id.Toolbar);
        toolbar.setTitle("");
        sourceName = getIntent().getStringExtra("sourceName");
        toolbar_title = findViewById(R.id.Toolbartitle);
        toolbar_title.setText(sourceName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private void newsData() {
        internetcheck();
        Comman.getNew().getNews(id, RetrofitClient.apikey).enqueue(new Callback<NewsList>() {
            @Override
            public void onResponse(Call<NewsList> call, Response<NewsList> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(NewsActivity.this, "Error" + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                list = response.body().articles;
                adpter = new NewsAdpter(NewsActivity.this, list);
                news_recycler.setAdapter(adpter);
                news_recycler.smoothScrollToPosition(0);
                newloading_layout.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<NewsList> call, Throwable t) {
                Toast.makeText(NewsActivity.this, "Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void internetcheck() {
        if (!Helper.isNetworkConnected(this)) {
            newointernetlayout.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            return;
        }
        newointernetlayout.setVisibility(View.GONE);
        newloading_layout.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);

    }
}
