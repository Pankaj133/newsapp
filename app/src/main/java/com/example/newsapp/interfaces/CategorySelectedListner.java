package com.example.newsapp.interfaces;

public interface CategorySelectedListner {

    void  onCategorySelected(String category);
}
