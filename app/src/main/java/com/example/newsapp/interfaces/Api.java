package com.example.newsapp.interfaces;

import com.example.newsapp.Model.NewsList;
import com.example.newsapp.Model.SourceList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

 @GET("sources")
 Call<SourceList> getsources(@Query("apiKey") String apiKey);

 @GET("sources")
 Call<SourceList> getSourcesByCategory(@Query("category")String category,@Query("apiKey") String apiKey);

 @GET("everything")
 Call<NewsList> getNews(@Query("sources") String source,@Query("apiKey") String apikey);




}
