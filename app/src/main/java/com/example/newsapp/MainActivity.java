package com.example.newsapp;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsapp.Adpters.CategoryAdapter;
import com.example.newsapp.Adpters.SourceAdpter;
import com.example.newsapp.Model.SourceList;
import com.example.newsapp.Model.Sources;
import com.example.newsapp.Refrofit.Comman;
import com.example.newsapp.Refrofit.Helper;
import com.example.newsapp.Refrofit.RetrofitClient;
import com.example.newsapp.interfaces.CategorySelectedListner;
import com.example.newsapp.interfaces.SourceeSelectedListner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements CategorySelectedListner, SourceeSelectedListner {

    private RecyclerView catrecyclerview, sourcerecyclerview;
    private SourceAdpter sourceAdpter;
    private CategoryAdapter categoryAdapter;
    private List<Sources> sourcesList = new ArrayList<>();
    private LinearLayout nointernetlayout, loading_layout, categorylayout;
    private RelativeLayout content;
    private TextView realoadbutton, category_selected;
    private ImageView removecategory;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sourcerecyclerview = findViewById(R.id.resource_recycler);
        catrecyclerview = findViewById(R.id.category_recycler);
        content = findViewById(R.id.content_main);
        loading_layout = findViewById(R.id.loadinglayout);
        nointernetlayout = findViewById(R.id.no_net);
        realoadbutton = findViewById(R.id.realloadbutton);
        removecategory = findViewById(R.id.remove_category);
        categorylayout = findViewById(R.id.category_layout);
        searchView = findViewById(R.id.serachview);
        category_selected = findViewById(R.id.categoryselected);
        sourceAdpter = new SourceAdpter(this,sourcesList,this);

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextFocusChangeListener((view, b) -> {
            if (b) {
                findViewById(R.id.toptext).setVisibility(View.GONE);
            } else {
                findViewById(R.id.toptext).setVisibility(View.VISIBLE);
            }
        });

        catwork();
        sourcework();
        Data();
        internetcheck();

        realoadbutton.setOnClickListener(v -> Data());

        removecategory.setOnClickListener(v -> {
            categorylayout.setVisibility(View.GONE);
            catrecyclerview.setVisibility(View.VISIBLE);
            Data();
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(Helper.isNetworkConnected(MainActivity.this))
                        sourceAdpter.getFilter().filter(s);
                else
                    Toast.makeText(MainActivity.this, "no internet", Toast.LENGTH_SHORT).show();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                if(Helper.isNetworkConnected(MainActivity.this))
                    sourceAdpter.getFilter().filter(s);
                return false;
            }
        });

    }

    private void sourcework() {
        sourcerecyclerview.setLayoutManager(new LinearLayoutManager(this));
        sourcerecyclerview.setHasFixedSize(true);
        //sourcerecyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

    }

    private void catwork() {
        catrecyclerview.setHasFixedSize(true);
        catrecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoryAdapter = new CategoryAdapter(this, this);
        catrecyclerview.setAdapter(categoryAdapter);

    }
    @Override
    public void onSourceSelected(Sources sources) {
        Intent i = new Intent(MainActivity.this, NewsActivity.class);
        i.putExtra("sourceId",sources.id);
        i.putExtra("sourceName",sources.name);
        startActivity(i);

    }

    private void Data() {
        internetcheck();
        Comman.getsource().getsources(RetrofitClient.apikey).enqueue(new Callback<SourceList>() {
            @Override
            public void onResponse(Call<SourceList> call, Response<SourceList> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Error" + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                sourcesList = response.body().sources;
                sourceAdpter.updateData(sourcesList);
                sourcerecyclerview.setAdapter(sourceAdpter);
                loading_layout.setVisibility(View.GONE);
                sourcerecyclerview.smoothScrollToPosition(0);
                content.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<SourceList> call, Throwable t) {

                Toast.makeText(MainActivity.this, "Error:" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onCategorySelected(String category) {

        catrecyclerview.setVisibility(View.GONE);
        categorylayout.setVisibility(View.VISIBLE);
        category_selected.setText(category + " News Source");
        Data(category.toLowerCase());
    }

    private void Data(String category) {
        internetcheck();
        Comman.getcategory().getSourcesByCategory(category, RetrofitClient.apikey).enqueue(new Callback<SourceList>() {
            @Override
            public void onResponse(Call<SourceList> call, Response<SourceList> response) {
                if (!response.isSuccessful()) {      //CHECK IF RESPONSE IS SUCCESSFUL
                    Toast.makeText(MainActivity.this, "Error: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                sourcesList = response.body().sources;
                sourceAdpter.updateData(sourcesList);
                sourcerecyclerview.smoothScrollToPosition(0);
                loading_layout.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<SourceList> call, Throwable t) {

                Toast.makeText(MainActivity.this, "Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void internetcheck() {
        if (!Helper.isNetworkConnected(this)) {
            nointernetlayout.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            return;
        }
        nointernetlayout.setVisibility(View.GONE);
        loading_layout.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);


    }
}
